# BuildingInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**location** | **list[float]** |  | [optional] 
**display_name** | **str** |  | [optional] 
**map_datatype** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**datatypes** | **list[str]** |  | [optional]
**maxima** | **list[float]** |  | [optional]
**minima** | **list[float]** |  | [optional]
**last_added** | **datetime** |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


