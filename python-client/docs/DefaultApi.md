# openapi_client.DefaultApi

All URIs are relative to *http://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_building**](DefaultApi.md#add_building) | **POST** /buildings | Add a building
[**delete_building**](DefaultApi.md#delete_building) | **DELETE** /buildings/{building} | Delete a building
[**get_building_info**](DefaultApi.md#get_building_info) | **GET** /buildings/{building} | Get information about the building
[**get_buildings**](DefaultApi.md#get_buildings) | **GET** /buildings | Get the list of buildings supported by this API
[**get_data**](DefaultApi.md#get_data) | **GET** /data/{building} | Get a list of data points as a JSON
[**post_data**](DefaultApi.md#post_data) | **POST** /data/{building} | Post a list of data points
[**update_building_info**](DefaultApi.md#update_building_info) | **PUT** /buildings/{building} | Update a building


# **add_building**
> add_building(building_info)

Add a building

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building_info = openapi_client.BuildingInfo() # BuildingInfo | Info about the building

    try:
        # Add a building
        api_instance.add_building(building_info)
    except ApiException as e:
        print("Exception when calling DefaultApi->add_building: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building_info** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Building added successfully |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**409** | Duplicate building |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_building**
> delete_building(building)

Delete a building

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building = 'building_example' # str | Name of the building

    try:
        # Delete a building
        api_instance.delete_building(building)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_building: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **str**| Name of the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Building successfully deleted |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Ressource does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_building_info**
> BuildingInfo get_building_info(building)

Get information about the building

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building = 'building_example' # str | Name of the building

    try:
        # Get information about the building
        api_response = api_instance.get_building_info(building)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_building_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **str**| Name of the building | 

### Return type

[**BuildingInfo**](BuildingInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Info about the building |  -  |
**404** | Building does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_buildings**
> list[str] get_buildings()

Get the list of buildings supported by this API

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    
    try:
        # Get the list of buildings supported by this API
        api_response = api_instance.get_buildings()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_buildings: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A JSON array of building names |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_data**
> Data get_data(building, first_date, last_date, datatypes=datatypes, timestep=timestep, response_format=response_format)

Get a list of data points as a JSON

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building = 'building_example' # str | Name of the building
first_date = '2017-07-21T17:32Z' # datetime | Date of the first data point
last_date = '2017-07-21T17:33Z' # datetime | Date of the last data point
datatypes = 'TensionV12 TensionV23 TensionV31 I1 I2 I3' # str | A space separated string list of the requested datatypes (optional)
timestep = 3.4 # float | Requested timestep, either 1 minute or 15 minutes. Default is 1 minute. (optional)
response_format = 'response_format_example' # str | Format of the response, either JSON or csv. Default is JSON. (optional)

    try:
        # Get a list of data points as a JSON
        api_response = api_instance.get_data(building, first_date, last_date, datatypes=datatypes, timestep=timestep, response_format=response_format)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **str**| Name of the building | 
 **first_date** | **datetime**| Date of the first data point | 
 **last_date** | **datetime**| Date of the last data point | 
 **datatypes** | **str**| A space separated string list of the requested datatypes | [optional]
 **timestep** | **float**| Requested timestep, either 1 minute or 15 minutes. Default is 1 minute. | [optional] 
 **response_format** | **str**| Format of the response, either JSON or csv. Default is JSON. | [optional] 

### Return type

[**Data**](Data.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Operation successful |  -  |
**400** | Bad request |  -  |
**403** | Unauthorized operation |  -  |
**404** | Ressource does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_data**
> post_data(building, data)

Post a list of data points

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building = 'building_example' # str | Name of the building
data = openapi_client.Data() # Data | JSON representing a dataframe

    try:
        # Post a list of data points
        api_instance.post_data(building, data)
    except ApiException as e:
        print("Exception when calling DefaultApi->post_data: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **str**| Name of the building | 
 **data** | [**Data**](Data.md)| JSON representing a dataframe |

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Operation successful |  -  |
**400** | Bad request |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**404** | Ressource does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_building_info**
> update_building_info(building, building_info)

Update a building

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    building = 'building_example' # str | Name of the building
building_info = openapi_client.BuildingInfo() # BuildingInfo | Info about the building

    try:
        # Update a building
        api_instance.update_building_info(building, building_info)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_building_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **str**| Name of the building | 
 **building_info** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Building successfully updated |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**404** | Ressource does not exist |  -  |
**409** | Duplicate building |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

