# coding: utf-8

from __future__ import absolute_import
import unittest
import datetime
import os
import pytz

from flask import json
from six import BytesIO

from openapi_server.models.building_info import BuildingInfo  # noqa: E501
from openapi_server.models.data import Data  # noqa: E501
from openapi_server.test import BaseTestCase

import auth_client.openapi_client as auth_client

class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs     
    """

    def _log_as_admin(self):
        # Configure auth-api
        auth_configuration = auth_client.Configuration(
            host = "openapi_server_auth:{}/1.0.0".format(os.environ['OAUTH2_PORT'])
        )

        # Login to get a token
        with auth_client.ApiClient(auth_configuration) as api_client:
            # Create an instance of the API class
            auth_api_instance = auth_client.DefaultApi(api_client)

            # Create info for login
            authentication_info = auth_client.AuthenticationInfo(
                os.environ['API_USER'],
                os.environ['API_PASSWD'],
                response_type='token')

            # Get the access token
            login = auth_api_instance.login(authentication_info)
            return login[2]["Location"].split('#')[1].split('&')[0].split('=')[1]

    def _log_as_unprivileged(self):
        auth_configuration = auth_client.Configuration(
            host = "openapi_server_auth:{}/1.0.0".format(os.environ['OAUTH2_PORT'])
        )

        # Login to get a token
        with auth_client.ApiClient(auth_configuration) as api_client:
            # Create an instance of the API class
            auth_api_instance = auth_client.DefaultApi(api_client)

            # Create info for login
            authentication_info = auth_client.AuthenticationInfo(
                "user_test_unprivileged",
                "topsecret",
                response_type='token')

            # Get the access token
            login = auth_api_instance.login(authentication_info)
            return login[2]["Location"].split('#')[1].split('&')[0].split('=')[1]

    def test_add_building(self):
        """Test case for add_building

        Add a building
        """

        building_info = {
          "name" : "buildy",
          "mapDatatype" : "power",
          "displayName" : "A new building",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "production",
          "datatypes": ["power"],
          "maxima": [15000],
          "minima": [0]
        }

        token = self._log_as_admin()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 201)

    def test_add_building_missing_field(self):
        """Tries to add a building but with missing fields
        """
        building_info = {
          "name" : "buildy",
          "mapDatatype" : "power",
          "location" : [ 53.2734, -7.7783 ],
          "datatypes": ["power"],
          "maxima": [15000],
          "minima": [0]
        }

        token = self._log_as_admin()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_add_building_incoherent_field1(self):
        """Tries to add a building but with incoherent fields
        """
        building_info = {
          "name" : "buildy",
          "mapDatatype" : "power",
          "location" : [ 53.2734, -7.7783 ],
          "datatypes": ["power"],
          "maxima": [15000, 1234, 897, 697],
          "minima": [0]
        }

        token = self._log_as_admin()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_add_building_incoherent_field2(self):
        """Tries to add a building but with incoherent fields
        """
        building_info = {
          "name" : "buildy",
          "mapDatatype" : "power",
          "displayName" : "A new building",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "production",
          "datatypes": ["power", "Tension12"],
          "maxima": [15000, 2],
          "minima": [0, 0]
        }

        token = self._log_as_admin()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_add_building_duplicate(self):
        """Tries to add a building that already exists
        """
        building_info = {
          "name" : "Kirikou",
          "mapDatatype" : "power",
          "displayName" : "A new building",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "production",
          "datatypes": ["power"],
          "maxima": [15000],
          "minima": [0]
        }

        token = self._log_as_admin()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 409)


    def test_add_building_unlogged(self):
        """ Tries to add a building while not being logged in
        """
        building_info = dict()
        headers = {
            'Content-Type': 'application/json'
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_add_building_fake_token(self):
        """ Tries to add a building with a fake token
        """
        building_info = dict()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer lEtmEiN'
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_add_building_forbidden(self):
        """ Tries to add a building without the specific scope
        """
        building_info = dict()
        token = self._log_as_unprivileged()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='POST',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 403)

    def test_delete_building(self):
        """Test case for delete_building

        Delete a building
        """
        token = self._log_as_admin()
        headers = { 
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8') + '\n Token seems valid: ' + token)

    def test_delete_building_non_existent(self):
        """Tries to delete a non-existent building
        """
        token = self._log_as_admin()
        headers = { 
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='IdontExist'),
            method='DELETE',
            headers=headers)
        self.assertEqual(response.status_code, 204)

    def test_add_delete_unlogged(self):
        """ Tries to delete a building while not being logged in
        """
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='DELETE')
        self.assertEqual(response.status_code, 401)

    def test_delete_building_fake_token(self):
        """ Tries to delete a building with a fake token
        """
        headers = {
            'Authorization': 'Bearer lEtmEiN'
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='DELETE',
            headers=headers)
        self.assertEqual(response.status_code, 401)

    def test_delete_building_forbidden(self):
        """ Tries to delete a building without the specific scope
        """
        token = self._log_as_unprivileged()
        headers = {
            'Authorization': f'Bearer {token}'
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='DELETE',
            headers=headers)
        self.assertEqual(response.status_code, 403)

    def test_get_building_info(self):
        """Test case for get_building_info

        Get information about the building
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_building_info_non_existent(self):
        """Tries to get the building info of a non existent building
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='IdontExist'),
            method='GET',
            headers=headers)
        self.assertEqual(response.status_code, 404)
        
    def test_get_buildings(self):
        """Test case for get_buildings

        Get the list of buildings supported by this API
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/buildings',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_data(self):
        """Test case for get_data

        Get a list of data points as a JSON
        """
        now = pytz.utc.localize(datetime.datetime.utcnow()).replace(second=0, microsecond=0)

        query_string = [('first_date', now - datetime.timedelta(minutes=15)),
                        ('last_date', now),
                        ('datatypes','voltage_12'),
                        ('timestep', 1),
                        ('response_format', 'json')]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Kirikou'),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


    def test_get_too_much_data(self):
        """Tris to get more than the max number days
        """
        now = pytz.utc.localize(datetime.datetime.utcnow()).replace(second=0, microsecond=0)

        query_string = [('first_date', now - datetime.timedelta(int(os.environ['MAX_DAYS']) + 1)),
                        ('last_date', now),
                        ('datatypes','voltage_12'),
                        ('timestep', 1),
                        ('response_format', 'json')]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Kirikou'),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assertEqual(response.status_code, 403)

    def test_get_too_much_data_admin(self):
        """Tris to get more than the max number days
        """
        now = pytz.utc.localize(datetime.datetime.utcnow()).replace(second=0, microsecond=0)
        token = self._log_as_admin()
        query_string = [('first_date', now - datetime.timedelta(int(os.environ['MAX_DAYS']) + 1)),
                        ('last_date', now),
                        ('datatypes','voltage_12'),
                        ('timestep', 1),
                        ('response_format', 'json')]
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Kirikou'),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8') + '\n Token seems valid: ' + token)

    def test_get_too_much_data_unprivileged(self):
        """Tris to get more than the max number days
        """
        now = pytz.utc.localize(datetime.datetime.utcnow()).replace(second=0, microsecond=0)
        token = self._log_as_unprivileged()
        query_string = [('first_date', now - datetime.timedelta(int(os.environ['MAX_DAYS']) + 1)),
                        ('last_date', now),
                        ('datatypes','voltage_12'),
                        ('timestep', 1),
                        ('response_format', 'json')]
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Kirikou'),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assertEqual(response.status_code, 403)

    def test_post_data(self):
        """Test case for set_building_datatype

        Set a list of data points
        """
        now = pytz.utc.localize(datetime.datetime.utcnow()).replace(second=0, microsecond=0)
        data = {
          "data" : [ [ 48.3 ], [ 549.77 ] ],
          "columns" : [ "power"],
          "index" : [ now + datetime.timedelta(days=1), now + datetime.timedelta(days=2)]
        }
        token = self._log_as_admin()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Namme'),
            method='POST',
            headers=headers,
            data=json.dumps(data),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8') + '\n Token seems valid: ' + token)

    def test_post_data_unlogged(self):
        """Tries to post data while being unlogged
        """
        data = dict()
        headers = { 
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Namme'),
            method='POST',
            headers=headers,
            data=json.dumps(data),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_post_data_fake_token(self):
        """Tries to post data with fake token
        """
        data = dict()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer lEtmEiN'
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Namme'),
            method='POST',
            headers=headers,
            data=json.dumps(data),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_post_data_unprivileged(self):
        """Tries to post data whithout the requred scope
        """
        data = dict()
        token = self._log_as_unprivileged()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/data/{building}'.format(building='Namme'),
            method='POST',
            headers=headers,
            data=json.dumps(data),
            content_type='application/json')
        self.assertEqual(response.status_code, 403)

    def test_update_building_info(self):
        """Test case for update_building_info

        Update a building
        """
        building_info = {
          "mapDatatype" : "active_power",
          "displayName" : "B28, B37, B65 and B26",
          "name" : "B28",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "consumption"
        }
        token = self._log_as_admin()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8') + '\n Token seems valid: ' + token)

    def test_updatte_building_incoherent_field(self):
        """Tries to update a building but with incoherent fields
        """
        building_info = {
          "mapDatatype" : "active_power",
          "displayName" : "B28, B37, B65 and B26",
          "name" : "B28",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "power"
        }
        token = self._log_as_admin()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_building_non_existent(self):
        """Tries to update that does not exist
        """
        building_info = {
          "mapDatatype" : "active_power",
          "displayName" : "B28, B37, B65 and B26",
          "name" : "B28",
          "location" : [ 53.2734, -7.7783 ],
          "type" : "consumption"
        }
        token = self._log_as_admin()
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='IdontExist'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_update_building_unlogged(self):
        """ Tries to update a building while not being logged in
        """
        building_info = dict()
        headers = {
            'Content-Type': 'application/json'
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_update_building_fake_token(self):
        """ Tries to update a building with a fake token
        """
        building_info = dict()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer lEtmEiN'
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 401)

    def test_update_building_forbidden(self):
        """ Tries to update a building without the specific scope
        """
        building_info = dict()
        token = self._log_as_unprivileged()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}'
        }
        response = self.client.open(
            '/1.0.0/buildings/{building}'.format(building='Kirikou'),
            method='PUT',
            headers=headers,
            data=json.dumps(building_info),
            content_type='application/json')
        self.assertEqual(response.status_code, 403)

if __name__ == '__main__':
    unittest.main()
