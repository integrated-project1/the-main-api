import logging

import connexion
from flask_testing import TestCase

from openapi_server.encoder import JSONEncoder
from openapi_server import db
from flask_sqlalchemy import SQLAlchemy
from openapi_server.__main__ import init_db
from openapi_server.controllers.security_controller_ import fetch_public_key

import json
import os

class BaseTestCase(TestCase):
    nbrDays = 3

    def create_app(self):
        logging.getLogger('connexion.operation').setLevel('ERROR')
        app = connexion.App(__name__, specification_dir='../openapi/')
        app.app.config['SQLALCHEMY_DATABASE_URI'] = \
f"""{os.environ['DATABASE_TYPE']}://\
{os.environ['DATABASE_USER']}:\
{os.environ['DATABASE_PASSWORD']}@\
{os.environ['DATABASE_HOST']}/\
{os.environ['DATABASE_NAME']}"""
        app.app.json_encoder = JSONEncoder
        app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
        with app.app.app_context():
            db.init_app(app.app)
        init_db(self.nbrDays)
        app.add_api('openapi.yaml', pythonic_params=True)
        fetch_public_key()
        return app.app