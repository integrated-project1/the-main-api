"""
Integrated software project - Electrical energy at a glance - Team 2

Database with "Concrete Table Inheritance"
see https://docs.sqlalchemy.org/en/13/orm/inheritance.html#concrete-table-inheritance
"""

from typing import List

from openapi_server import db

from sqlalchemy import Column, String, Float, DateTime

from openapi_server.models.building_info import BuildingInfo

class DataConsumption(db.Model):
    __tablename__ = 'consumption'

    building = db.Column(db.String(10), primary_key=True, nullable=False)
    datetime = db.Column(db.DateTime, primary_key=True, nullable=False)
    voltage_12 = db.Column(db.Float)
    voltage_23 = db.Column(db.Float)
    voltage_31 = db.Column(db.Float)
    current_1 = db.Column(db.Float)
    current_2 = db.Column(db.Float)
    current_3 = db.Column(db.Float)
    active_power = db.Column(db.Float)
    reactive_power = db.Column(db.Float)
    cos_phi = db.Column(db.Float)


class DataProduction(db.Model):
    __tablename__ = 'production'

    building = db.Column(db.String(10), primary_key=True, nullable=False)
    datetime = db.Column(db.DateTime, primary_key=True, nullable=False)
    power = db.Column(db.Float)
