"""
Integrated software project - Electrical energy at a glance - Team 2

Database with "Concrete Table Inheritance"
see https://docs.sqlalchemy.org/en/13/orm/inheritance.html#concrete-table-inheritance
"""
import six

from typing import List

from openapi_server import db

from sqlalchemy import Column, String, Float, DateTime

from openapi_server.models.building_info import BuildingInfo

from openapi_server.models.data_db import DataConsumption, DataProduction

class BuildingBase(db.Model):
    __tablename__ = 'buildings'
    
    name = db.Column(db.String(10), primary_key=True)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    display_name = db.Column(db.String(25))
    map_datatype = db.Column(db.String(25))
    type = db.Column(db.String(25))
    last_added = db.Column(db.DateTime)

    __mapper_args__ = {
        'polymorphic_identity': 'building',
        'polymorphic_on': type,
    }


class BuildingConsumption(BuildingBase):
    __tablename__ = None

    __mapper_args__ = {
        'polymorphic_identity': 'consumption'
    }

    datatypes = {
        'voltage_12': DataConsumption.voltage_12,
        'voltage_23': DataConsumption.voltage_23,
        'voltage_31': DataConsumption.voltage_31,
        'current_1': DataConsumption.current_1,
        'current_2': DataConsumption.current_2,
        'current_3': DataConsumption.current_3,
        'active_power': DataConsumption.active_power,
        'reactive_power': DataConsumption.reactive_power,
        'cos_phi': DataConsumption.cos_phi
    }

    voltage_12_max = db.Column(db.Float)
    voltage_12_min = db.Column(db.Float)
    voltage_23_max = db.Column(db.Float)
    voltage_23_min = db.Column(db.Float)
    voltage_31_max = db.Column(db.Float)
    voltage_31_min = db.Column(db.Float)
    current_1_max = db.Column(db.Float)
    current_1_min = db.Column(db.Float)
    current_2_max = db.Column(db.Float)
    current_2_min = db.Column(db.Float)
    current_3_max = db.Column(db.Float)
    current_3_min = db.Column(db.Float)
    active_power_max = db.Column(db.Float)
    active_power_min = db.Column(db.Float)
    reactive_power_max = db.Column(db.Float)
    reactive_power_min = db.Column(db.Float)
    cos_phi_max = db.Column(db.Float)
    cos_phi_min = db.Column(db.Float)
    
    

class BuildingProduction(BuildingBase):
    __tablename__ = None

    __mapper_args__ = {
        'polymorphic_identity': 'production'
    }

    datatypes = {
        'power': DataProduction.power,
    }

    power_max = db.Column(db.Float)
    power_min = db.Column(db.Float)

def building_from_info(building_info):
    """ Some kind of factory that builds, from a BuildingInfo object, the corresponding db object

    :param building_info: Info about the building
    :type building_info: BuildingInfo

    :rtype: BuildingConsumption | BuildingProduction
    """
    assert isinstance(building_info, BuildingInfo)

    if building_info.type == "consumption":
        Table = BuildingConsumption
    elif building_info.type == "production":
        Table = BuildingProduction
    else:
        raise ValueError

    fields = {
        "name": building_info.name,
        "longitude": building_info.location[0] if isinstance(building_info.location, List) else None,
        "latitude": building_info.location[1] if isinstance(building_info.location, List) else None,
        "display_name": building_info.display_name,
        "map_datatype": building_info.map_datatype,
        "type": building_info.type,
        "last_added": building_info.last_added
    }

    datatypes = building_info.datatypes
    
    # May raise KeyError, IndexError
    for i in range(len(datatypes)):
        fields[datatypes[i] + "_max"] = building_info.maxima[i]
        fields[datatypes[i] + "_min"] = building_info.minima[i]

    # May raise TypeError
    return Table(**fields)

def info_from_building(building):
    """ Returns a BuildingInfo object from a Building{Consumption,Production} object

    :param building_info: Building{Consumption,Production} object
    :type building_info: BuildingConsumption | BuildingProduction

    :rtype: BuildingInfo
    """
    assert isinstance(building, BuildingConsumption) or isinstance(building, BuildingProduction)

    datatypes = []
    maxima = []
    minima = []

    for attr, _ in six.iteritems(building.datatypes):
        datatypes.append(attr)
        maxima.append(getattr(building, attr + "_max"))
        minima.append(getattr(building, attr + "_min"))

    try:
        return BuildingInfo(
            name = building.name,
            location = [building.longitude, building.latitude] if building.longitude is not None and building.latitude is not None else None,
            display_name = building.display_name,
            map_datatype = building.map_datatype,
            type = building.type,
            last_added = building.last_added,
            datatypes = datatypes,
            maxima = maxima,
            minima = minima
        )
    except TypeError:
        return None
