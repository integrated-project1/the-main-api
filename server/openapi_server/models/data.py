# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model_ import Model
from openapi_server import util


class Data(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, columns=None, index=None, data=None):  # noqa: E501
        """Data - a model defined in OpenAPI

        :param columns: The columns of this Data.  # noqa: E501
        :type columns: List[str]
        :param index: The index of this Data.  # noqa: E501
        :type index: List[str]
        :param data: The data of this Data.  # noqa: E501
        :type data: List[List[float]]
        """
        self.openapi_types = {
            'columns': List[str],
            'index': List[str],
            'data': List[List[float]]
        }

        self.attribute_map = {
            'columns': 'columns',
            'index': 'index',
            'data': 'data'
        }

        self._columns = columns
        self._index = index
        self._data = data

    @classmethod
    def from_dict(cls, dikt) -> 'Data':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Data of this Data.  # noqa: E501
        :rtype: Data
        """
        return util.deserialize_model(dikt, cls)

    @property
    def columns(self):
        """Gets the columns of this Data.


        :return: The columns of this Data.
        :rtype: List[str]
        """
        return self._columns

    @columns.setter
    def columns(self, columns):
        """Sets the columns of this Data.


        :param columns: The columns of this Data.
        :type columns: List[str]
        """
        if columns is None:
            raise ValueError("Invalid value for `columns`, must not be `None`")  # noqa: E501

        self._columns = columns

    @property
    def index(self):
        """Gets the index of this Data.


        :return: The index of this Data.
        :rtype: List[str]
        """
        return self._index

    @index.setter
    def index(self, index):
        """Sets the index of this Data.


        :param index: The index of this Data.
        :type index: List[str]
        """
        if index is None:
            raise ValueError("Invalid value for `index`, must not be `None`")  # noqa: E501

        self._index = index

    @property
    def data(self):
        """Gets the data of this Data.


        :return: The data of this Data.
        :rtype: List[List[float]]
        """
        return self._data

    @data.setter
    def data(self, data):
        """Sets the data of this Data.


        :param data: The data of this Data.
        :type data: List[List[float]]
        """
        if data is None:
            raise ValueError("Invalid value for `data`, must not be `None`")  # noqa: E501

        self._data = data
