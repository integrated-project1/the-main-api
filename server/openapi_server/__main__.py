#!/usr/bin/env python3

import datetime
import random
import os
import pytz

from openapi_server import app
from openapi_server import db
from openapi_server.models.data_db import DataConsumption, DataProduction
from openapi_server.models.building_db import BuildingConsumption, BuildingProduction

import connexion

#constant for aware datetime
BRUSSELS = pytz.timezone('Europe/Brussels')



def init_db(nbrDays=60):
    """
    Fill the db

    :param nbrDays: number of days of data to fill
    :type nbrDays: int
    """
	# Adding a building
    from openapi_server import db
    from openapi_server.models.data_db import DataConsumption, DataProduction
    from openapi_server.models.building_db import BuildingConsumption, BuildingProduction
    import datetime
    import random
    db.drop_all()
    db.create_all()
    db.session.commit()
    Namme = BuildingProduction(
        name="Namme",
        longitude=0.5,
        latitude=-10.7,
        display_name="euh",
        map_datatype="tension",
        type ="production",
        last_added = BRUSSELS.localize(datetime.datetime(2020, 4, 20, 0, 0, 0)),
        power_max = 55.5,
        power_min = 66.7
    )
    Kirikou = BuildingConsumption(
        name="Kirikou",
        longitude=0.5,
        latitude=-987,
        display_name="euh",
        map_datatype="tension",
        type ="consumption",
        voltage_12_max = 55.5,
        voltage_12_min = 55.5,
        voltage_23_max = 55.5,
        voltage_23_min = 55.5,
        voltage_31_max = 55.5,
        voltage_31_min = 55.5,
        current_1_max = 55.5,
        current_1_min = 55.5,
        current_2_max = 77.5,
        current_2_min = 55.5,
        current_3_max = 55.5,
        current_3_min = 55.5,
        active_power_max = 55.5,
        active_power_min = 55.5,
        reactive_power_max = 99.5,
        reactive_power_min = 55.5,
        cos_phi_max = 55.5,
        cos_phi_min = 55.5
    )

    db.session.add(Namme)
    db.session.add(Kirikou)
    db.session.commit()

    now = datetime.datetime.now(tz=BRUSSELS).replace(second=0, microsecond=0)
    then = now - datetime.timedelta(days=nbrDays)
    passed_minutes = int((now - then).total_seconds() // 60)
    cons = [
        DataConsumption(
            building='Kirikou',
            datetime=now - datetime.timedelta(minutes=i),
            voltage_12=random.random(),
            voltage_23=random.random(),
            voltage_31=random.random(),
            current_1=random.random(),
            current_2=random.random(),
            current_3=random.random(),
            active_power=random.random(),
            reactive_power=random.random(),
            cos_phi=random.random()
        )
        for i in range(passed_minutes)] 
    prod = [
        DataProduction(
            building='Namme',
            datetime=now - datetime.timedelta(minutes=i),
            power=random.random()
        )
        for i in range(passed_minutes)]  # 8 PV stations 24 hours
    db.session.add_all(cons)
    db.session.add_all(prod)
    db.session.commit()



if __name__ == '__main__':
    if os.environ['INIT_DB'].upper() == "YES":
        init_db(int(os.environ['NB_DAYS']))
    app.run(port=os.environ['DATA_PORT'])
