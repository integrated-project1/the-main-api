from typing import List

import jwt
import time
import os

import auth_client.openapi_client as auth_client

def fetch_public_key():
    """ Requests the authentication server's public key
    """
    configuration = auth_client.Configuration(host = "openapi_server_auth:{}/1.0.0".format(os.environ['OAUTH2_PORT']))

    with auth_client.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = auth_client.DefaultApi(api_client)

        try:
            globals()['PUBLIC_KEY'] = api_instance.get_key()[0]['key']
        except (auth_client.ApiException, KeyError) as e:
            exit(1)

def decode_token(token):
    """
    Validate and decode token.
    Returned value will be passed in 'token_info' parameter of your operation function, if there is one.
    'sub' or 'uid' will be set in 'user' parameter of your operation function, if there is one.
    'scope' or 'scopes' will be passed to scope validation function.

    :param token Token provided by Authorization header
    :type token: str
    :return: Decoded token information or None if token is invalid
    :rtype: dict | None
    """
    try:
        headers = jwt.get_unverified_header(token)
        payload = jwt.decode(token, PUBLIC_KEY, algorithms=[os.environ["JWT_ALGORITHM"]])

        # In order to be compliant with Connexion and RFC 7662
        payload['active'] = True if payload['exp'] < int(time.time()) else False
        return payload
    except:
        return None
