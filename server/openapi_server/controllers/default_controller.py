import connexion
import six
import os
import datetime
import numpy as np
import pandas as pd
import pytz

from typing import List
from flask_sqlalchemy import SQLAlchemy

from openapi_server.models.building_info import BuildingInfo  # noqa: E501
from openapi_server.models.data import Data  # noqa: E501
from openapi_server import util

from openapi_server import db
from openapi_server.models.data_db import DataConsumption, DataProduction
from openapi_server.models.building_db import BuildingBase, BuildingConsumption, BuildingProduction, building_from_info, info_from_building

from openapi_server.controllers.security_controller_ import decode_token



def _get_building(building_name):
    """Helper function to fetch a building from the database

    :param building_name: Name of the building
    :type building_name: str

    :rtype: BuildingConsumption | BuildingProduction | None
    """
    assert isinstance(building_name, str)
    return BuildingBase.query.get(building_name)

def _building_info_sanity(building_info):
    """Sanity checker for a building info in regards to the semantics of this api. Connexion already checks the parameters' types hence we must check the array size manually.
    Returns an error message or None if none is malformed.

    :param building_name: Info of the building
    :type building_name: BuildingInfo

    :rtype: str
    """
    assert isinstance(building_info, BuildingInfo)

    def format_message(field, message):
        assert isinstance(field, str) and isinstance(message, str)
        return f"invalid field '{building_info.attribute_map[field]}': {message}"

    if building_info.location is not None and len(building_info.location) != 2:
        return format_message("location", "should be of size 2")
    if building_info.datatypes is not None:
        # If datatypes is not specified, maxima and minma are ignored
        building_datatypes = set(building_info.datatypes)
        production_datatypes = set(list(BuildingProduction.datatypes.keys()))
        consumption_datatypes = set(list(BuildingConsumption.datatypes.keys()))
        if not (building_datatypes == production_datatypes or building_datatypes == consumption_datatypes):
            return format_message("datatypes", "unsupported datatypes")

        if len(building_datatypes) != len(building_info.datatypes):
            return format_message("datatypes", "duplicate datatype")
        
        for attr in ("maxima", "minima"):
            extrema = getattr(building_info, attr)
            if extrema is not None and len(building_datatypes) != len(extrema):
                return format_message(attr, f"datatypes and {attr} should be of the same size")

    return None

def add_building(body):  # noqa: E501
    """Add a building

     # noqa: E501

    :param body: Info about the building
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        building_info = BuildingInfo.from_dict(connexion.request.get_json())  # noqa: E501

    for attr, _ in six.iteritems(building_info.openapi_types):
        value = getattr(building_info, attr)
        if value is None and attr != "last_added":
            return f"'{building_info.attribute_map[attr]}' field is a required property", 400

    ret = _building_info_sanity(building_info)
    if ret is not None:
        return ret, 400

    # At this point the only error that could occur is incompatible building type and datatypes
    try:
        building = building_from_info(building_info)
    except (ValueError, KeyError, IndexError, TypeError):
        return "Datatypes and building type are not compatible", 400

    # There might be a duplicata - should be done last to avoid disk I/O
    if _get_building(building_info.name) is not None:
        return "Duplicate building", 409

    db.session.add(building)
    db.session.commit()
    return 'Building successfully added', 201 


def delete_building(building):  # noqa: E501
    """Delete a building

     # noqa: E501

    :param building: Name of the building
    :type building: str

    :rtype: None
    """
    building = _get_building(building)
    if building is None:
        return 'Building does not exist', 204

    db.session.delete(building)
    db.session.commit()
    return 'Building successfully deleted', 200

def get_building_info(building):  # noqa: E501
    """Get information about the building

     # noqa: E501

    :param building: Name of the building
    :type building: str

    :rtype: BuildingInfo
    """
    building = _get_building(building)
    if building is None:
        return "Ressource not found", 404
    return info_from_building(building), 200


def get_buildings():  # noqa: E501
    """Get the list of buildings supported by this API

     # noqa: E501


    :rtype: List[str]
    """
    buildings = BuildingBase.query.all()
    return list(map(lambda x : x.name, buildings)), 200


def get_data(building, first_date, last_date, datatypes=None, timestep=1, response_format="JSON"):  # noqa: E501
    """Get a list of data points as a JSON

     # noqa: E501

    :param building: Name of the building
    :type building: str
    :param first_date: Date of the first data point
    :type first_date: str
    :param last_date: Date of the last data point
    :type last_date: str
    :param datatypes: A space separated string list of the requested datatypes
    :type datatypes: str
    :param timestep: Requested timestep, either 1 minute or 15 minutes. Default is 1 minute.
    :type timestep: 
    :param response_format: Format of the response, either JSON or csv. Default is JSON.
    :type response_format: str

    :rtype: Data
    """
    try:
        first_date, last_date = tuple(map(
            util.deserialize_datetime,
            (first_date, last_date)
        ))
    except:
        return "Invalid date format", 400
    
    # Checking awareness of the dates
    first_date, last_date = tuple(map(
        lambda date : date.astimezone(pytz.UTC)
        if date.tzinfo is not None and date.tzinfo.utcoffset(date) is not None
        else pytz.utc.localize(date),
        (first_date, last_date)
    ))

    # Protecting data manually
    MAX_DAYS = int(os.environ['MAX_DAYS'])
    now = pytz.utc.localize(datetime.datetime.utcnow())
    if (now - first_date).days > MAX_DAYS or (now - last_date).days > MAX_DAYS:
        try:
            if 'read:data' not in decode_token(connexion.request.headers['Authorization'].split()[1])['scope'].split():
                raise Exception('Someone is trying to steal data!')
        except (KeyError, Exception):
            return 'You are limited to 30 days of data', 403

    # Checking the timestep
    if timestep not in [1, 15]:
        return "Invalid timestep", 400

    # Checking the response_format
    response_format = response_format.upper()
    if response_format not in ['JSON', 'CSV']:
        return "Invalid response format", 400

    # Looking for the building - checking this as late as possible to avoid disk I/O
    building = _get_building(building)
    if building is None:
        return "Ressource not found", 404

     # Checking datatypes
    Table = DataProduction if building.type == 'production' else DataConsumption
    all_datatypes = set(list(building.datatypes.keys()))
    request_datatypes = set(datatypes.split()) if datatypes is not None else all_datatypes

    if not request_datatypes.issubset(all_datatypes) or len(request_datatypes) == 0:
        return "Invalid datatypes", 400


    # At this point, the query is valid
    query = Table.query\
        .with_entities(*list(map(lambda x: building.datatypes[x], list(request_datatypes))), Table.datetime)\
        .filter(Table.datetime.between(first_date, last_date))\
        .filter(Table.building == building.name)\
        .order_by(Table.datetime)
    df = pd.read_sql(query.statement, db.engine, index_col='datetime')
    df.tz_localize('UTC') # Adding timezone info

    if timestep == 15:
        df.index = pd.DatetimeIndex(df.index)
        df = df.resample(f'{timestep}min', origin='start_day').mean()

    if response_format == 'JSON':
        df = df.replace({np.nan: None}) # JSON does not support NaN
        return df.to_dict(orient='split'), 200
    else: # csv
        return df.to_csv(), 200

def post_data(building, body):  # noqa: E501
    """Set a list of data points

     # noqa: E501

    :param building: Name of the building
    :type building: str
    :param body: Info about the building
    :type body: dict | bytes

    :rtype: None
    """
    building = _get_building(building)
    if building is None:
        return "Ressource not Found", 404

    try:
        # This may fail if the json is malformed
        df = pd.read_json(connexion.request.data, orient='split')
    except:
        return "Request body is malformed", 400

    request_columns = set(df.columns)

    # Adding building column
    df['building'] = building.name


    try:
        # df.to_sql does not use model hence we must check the columns manually
        # This mail fail if datetimes (indexes) are malformed
        if request_columns == set(list(BuildingConsumption.datatypes.keys())):
            df.to_sql('consumption', con=db.engine, index_label='datetime', if_exists='append')
        elif request_columns == set(list(BuildingProduction.datatypes.keys())):
            df.to_sql('production', con=db.engine, index_label='datetime', if_exists='append')
        else:
            raise ValueError()
    except:
        return "Invalid columns or index format", 400

    # At this point, the data was successfully added - the building exists and the datetimes (indexes) are well formed
    # Assuming no crazy interleaving occurs, the following is safe
    try:
        building.last_added = df.index[-1]
    except IndexError:
        pass
    db.session.commit()
    return 'Data successfully added', 200


def update_building_info(building, body):  # noqa: E501
    """Update a building

     # noqa: E501

    :param building: Name of the building
    :type building: str
    :param body: Info about the building
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        new_building_info = BuildingInfo.from_dict(connexion.request.get_json())  # noqa: E501

    ret = _building_info_sanity(new_building_info)
    if ret is not None:
        return ret, 400


    # We'll go the ugly way for convenience: old_building -> old_building_info + new_building_info -> new_building -> delete + add + commit
    old_building = _get_building(building)

    if old_building is None:
        return 'Ressource not found', 404

    old_building_info = info_from_building(old_building)

    # Updating building info
    for attr, _ in six.iteritems(old_building_info.openapi_types):
        new_value = getattr(new_building_info, attr)
        if new_value is not None and attr != "name":
            setattr(old_building_info, attr, new_value)

    try:
        new_building = building_from_info(old_building_info)
    except (ValueError, KeyError, IndexError, TypeError):
        return "Datatypes and building type are not compatible", 400    

    db.session.delete(old_building)
    db.session.add(new_building)
    db.session.commit()
    return 'Building successfully modified', 200
