import connexion
import os
from flask_sqlalchemy import SQLAlchemy

from openapi_server import encoder
from openapi_server.controllers.security_controller_ import fetch_public_key

app = connexion.App(__name__, specification_dir='./openapi/')
app.app.config['SQLALCHEMY_DATABASE_URI'] = \
f"""{os.environ['DATABASE_TYPE']}://\
{os.environ['DATABASE_USER']}:\
{os.environ['DATABASE_PASSWORD']}@\
{os.environ['DATABASE_HOST']}/\
{os.environ['DATABASE_NAME']}"""

db = SQLAlchemy(app.app)


app.app.json_encoder = encoder.JSONEncoder
app.add_api('openapi.yaml',
            arguments={'title': 'Electrical energy at a glance authentication service API'},
            pythonic_params=True)

db.create_all()
db.session.commit()

fetch_public_key()
