# The Main API

## Some thoughts on the data-endpoint requirements

For each tab of the website the requirement is __DIFFERENT!__:

- Data- & Map-tab: __ONE__ datatype of __ONE__ building (Consumption and Production)
- For the Phasor-tab: __ALL__ datatypes of __ONE__ building (Consumption only, because you need tri-phase)
- For the Export-tab: __LIST OF__ datatypes of __ONE__ building (Consumption and Production)

If we implement the API so that it works for the Export-tab (client can request a list of datatypes for one building), the Data-, Map-, & Phasor-tab will work automatically (we could add the "all" datatype for the phasor).

So either we make one single data-endpoint which is used by all the tabs, __OR__ we make 3 (or more) data-endpoints each specialized for one purpose.

- Consumption datatypes: `['voltage_12', 'voltage_23', 'voltage_31', 'current_1', 'current_2', 'current_3', 'active_power', 'reactive_power', 'cos_phi'] + index=datetime`

- Production datatypes: `['power'] + index=datetime`
