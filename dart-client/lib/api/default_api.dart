part of openapi.api;



class DefaultApi {
  final ApiClient apiClient;

  DefaultApi([ApiClient _apiClient]) : apiClient = _apiClient ?? defaultApiClient;

  /// Add a building with HTTP info returned
  ///
  /// 
  Future addBuildingWithHttpInfo(BuildingInfo buildingInfo) async {
    Object postBody = buildingInfo;

    // verify required params are set
    if(buildingInfo == null) {
     throw ApiException(400, "Missing required param: buildingInfo");
    }

    // create path and map variables
    String path = "/buildings".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add a building
  ///
  ///BuildingInfo buildingInfo  (required):
  ///     Info about the building
  /// 
  Future addBuilding(BuildingInfo buildingInfo) async {
    Response response = await addBuildingWithHttpInfo(buildingInfo);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Delete a building with HTTP info returned
  ///
  /// 
  Future deleteBuildingWithHttpInfo(String building) async {
    Object postBody;

    // verify required params are set
    if(building == null) {
     throw ApiException(400, "Missing required param: building");
    }

    // create path and map variables
    String path = "/buildings/{building}".replaceAll("{format}","json").replaceAll("{" + "building" + "}", building.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'DELETE',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Delete a building
  ///
  ///String building  (required):
  ///     Name of the building
  /// 
  Future deleteBuilding(String building) async {
    Response response = await deleteBuildingWithHttpInfo(building);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get information about the building with HTTP info returned
  ///
  /// 
  Future<Response> getBuildingInfoWithHttpInfo(String building) async {
    Object postBody;

    // verify required params are set
    if(building == null) {
     throw ApiException(400, "Missing required param: building");
    }

    // create path and map variables
    String path = "/buildings/{building}".replaceAll("{format}","json").replaceAll("{" + "building" + "}", building.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get information about the building
  ///
  ///String building  (required):
  ///     Name of the building
  /// 
  Future<BuildingInfo> getBuildingInfo(String building) async {
    Response response = await getBuildingInfoWithHttpInfo(building);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BuildingInfo') as BuildingInfo;
    } else {
      return null;
    }
  }

  /// Get the list of buildings supported by this API with HTTP info returned
  ///
  /// 
  Future<Response> getBuildingsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/buildings".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get the list of buildings supported by this API
  ///
  /// 
  Future<List<String>> getBuildings() async {
    Response response = await getBuildingsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<String>') as List).map((item) => item as String).toList();
    } else {
      return null;
    }
  }

  /// Get a list of data points as a JSON with HTTP info returned
  ///
  /// 
  Future<Response> getDataWithHttpInfo(String building, DateTime firstDate, DateTime lastDate, { String datatypes, num timestep, String responseFormat }) async {
    Object postBody;

    // verify required params are set
    if(building == null) {
     throw ApiException(400, "Missing required param: building");
    }
    if(firstDate == null) {
     throw ApiException(400, "Missing required param: firstDate");
    }
    if(lastDate == null) {
     throw ApiException(400, "Missing required param: lastDate");
    }

    // create path and map variables
    String path = "/data/{building}".replaceAll("{format}","json").replaceAll("{" + "building" + "}", building.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};
      queryParams.addAll(_convertParametersForCollectionFormat("", "first_date", firstDate));
      queryParams.addAll(_convertParametersForCollectionFormat("", "last_date", lastDate));
    if(datatypes != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "datatypes", datatypes));
    }
    if(timestep != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "timestep", timestep));
    }
    if(responseFormat != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "response_format", responseFormat));
    }

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get a list of data points as a JSON
  ///
  ///String building  (required):
  ///     Name of the building
  ///DateTime firstDate  (required):
  ///     Date of the first data point
  ///DateTime lastDate  (required):
  ///     Date of the last data point
  ///String datatypes :
  ///     A space separated string list of the requested datatypes
  ///num timestep :
  ///     Requested timestep, either 1 minute or 15 minutes. Default is 1 minute.
  ///String responseFormat :
  ///     Format of the response, either JSON or csv. Default is JSON.
  /// 
  Future<Data> getData(String building, DateTime firstDate, DateTime lastDate, { String datatypes, num timestep, String responseFormat }) async {
    Response response = await getDataWithHttpInfo(building, firstDate, lastDate,  datatypes: datatypes, timestep: timestep, responseFormat: responseFormat );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Data') as Data;
    } else {
      return null;
    }
  }

  /// Post a list of data points with HTTP info returned
  ///
  /// 
  Future postDataWithHttpInfo(String building, Data data) async {
    Object postBody = data;

    // verify required params are set
    if(building == null) {
     throw ApiException(400, "Missing required param: building");
    }
    if(data == null) {
     throw ApiException(400, "Missing required param: data");
    }

    // create path and map variables
    String path = "/data/{building}".replaceAll("{format}","json").replaceAll("{" + "building" + "}", building.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Post a list of data points
  ///
  ///String building  (required):
  ///     Name of the building
  ///Data data  (required):
  ///     JSON representing a dataframe
  /// 
  Future postData(String building, Data data) async {
    Response response = await postDataWithHttpInfo(building, data);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Update a building with HTTP info returned
  ///
  /// 
  Future updateBuildingInfoWithHttpInfo(String building, BuildingInfo buildingInfo) async {
    Object postBody = buildingInfo;

    // verify required params are set
    if(building == null) {
     throw ApiException(400, "Missing required param: building");
    }
    if(buildingInfo == null) {
     throw ApiException(400, "Missing required param: buildingInfo");
    }

    // create path and map variables
    String path = "/buildings/{building}".replaceAll("{format}","json").replaceAll("{" + "building" + "}", building.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Update a building
  ///
  ///String building  (required):
  ///     Name of the building
  ///BuildingInfo buildingInfo  (required):
  ///     Info about the building
  /// 
  Future updateBuildingInfo(String building, BuildingInfo buildingInfo) async {
    Response response = await updateBuildingInfoWithHttpInfo(building, buildingInfo);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

}
