part of openapi.api;

class Data {
  
  List<String> columns = const [];
  
  List<String> index = const [];
  
  List<List<double>> data = const [];

  Data({
    @required this.columns,
    @required this.index,
    @required this.data,
  });

  @override
  String toString() {
    return 'Data[columns=$columns, index=$index, data=$data, ]';
  }

  Data.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    columns = (json['columns'] == null) ?
      null :
      (json['columns'] as List).cast<String>();
    index = (json['index'] == null) ?
      null :
      (json['index'] as List).cast<String>();
    data = (json['data'] == null) ?
      null :
      (json['data'] as List).map(
        (e) => e == null ? null :
          (e as List).cast<double>()
      ).toList();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    if (columns != null)
      json['columns'] = columns;
    if (index != null)
      json['index'] = index;
    if (data != null)
      json['data'] = data;
    return json;
  }

  static List<Data> listFromJson(List<dynamic> json) {
    return json == null ? List<Data>() : json.map((value) => Data.fromJson(value)).toList();
  }

  static Map<String, Data> mapFromJson(Map<String, dynamic> json) {
    final map = Map<String, Data>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Data.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Data-objects as value to a dart map
  static Map<String, List<Data>> mapListFromJson(Map<String, dynamic> json) {
    final map = Map<String, List<Data>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = Data.listFromJson(value);
      });
    }
    return map;
  }
}

