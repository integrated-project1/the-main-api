part of openapi.api;

class BuildingInfo {
  
  String name;
  
  List<double> location = const [];
  
  String displayName;
  
  String mapDatatype;
  
  String type;
  
  List<String> datatypes = const [];
  
  List<double> maxima = const [];
  
  List<double> minima = const [];
  
  DateTime lastAdded;

  BuildingInfo({
    this.name,
    this.location = const [],
    this.displayName,
    this.mapDatatype,
    this.type,
    this.datatypes = const [],
    this.maxima = const [],
    this.minima = const [],
    this.lastAdded,
  });

  @override
  String toString() {
    return 'BuildingInfo[name=$name, location=$location, displayName=$displayName, mapDatatype=$mapDatatype, type=$type, datatypes=$datatypes, maxima=$maxima, minima=$minima, lastAdded=$lastAdded, ]';
  }

  BuildingInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    location = (json['location'] == null) ?
      null :
      (json['location'] as List).cast<double>();
    displayName = json['displayName'];
    mapDatatype = json['mapDatatype'];
    type = json['type'];
    datatypes = (json['datatypes'] == null) ?
      null :
      (json['datatypes'] as List).cast<String>();
    maxima = (json['maxima'] == null) ?
      null :
      (json['maxima'] as List).cast<double>();
    minima = (json['minima'] == null) ?
      null :
      (json['minima'] as List).cast<double>();
    lastAdded = (json['last_added'] == null) ?
      null :
      DateTime.parse(json['last_added']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (location != null)
      json['location'] = location;
    if (displayName != null)
      json['displayName'] = displayName;
    if (mapDatatype != null)
      json['mapDatatype'] = mapDatatype;
    if (type != null)
      json['type'] = type;
    if (datatypes != null)
      json['datatypes'] = datatypes;
    if (maxima != null)
      json['maxima'] = maxima;
    if (minima != null)
      json['minima'] = minima;
    if (lastAdded != null)
      json['last_added'] = lastAdded == null ? null : lastAdded.toUtc().toIso8601String();
    return json;
  }

  static List<BuildingInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<BuildingInfo>() : json.map((value) => BuildingInfo.fromJson(value)).toList();
  }

  static Map<String, BuildingInfo> mapFromJson(Map<String, dynamic> json) {
    final map = Map<String, BuildingInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = BuildingInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of BuildingInfo-objects as value to a dart map
  static Map<String, List<BuildingInfo>> mapListFromJson(Map<String, dynamic> json) {
    final map = Map<String, List<BuildingInfo>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = BuildingInfo.listFromJson(value);
      });
    }
    return map;
  }
}

