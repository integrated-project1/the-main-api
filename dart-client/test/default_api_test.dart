import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for DefaultApi
void main() {
  var instance = DefaultApi();

  group('tests for DefaultApi', () {
    // Add a building
    //
    //Future addBuilding(BuildingInfo buildingInfo) async 
    test('test addBuilding', () async {
      // TODO
    });

    // Delete a building
    //
    //Future deleteBuilding(String building) async 
    test('test deleteBuilding', () async {
      // TODO
    });

    // Get information about the building
    //
    //Future<BuildingInfo> getBuildingInfo(String building) async 
    test('test getBuildingInfo', () async {
      // TODO
    });

    // Get the list of buildings supported by this API
    //
    //Future<List<String>> getBuildings() async 
    test('test getBuildings', () async {
      // TODO
    });

    // Get a list of data points as a JSON
    //
    //Future<Data> getData(String building, DateTime firstDate, DateTime lastDate, { String datatypes, num timestep, String responseFormat }) async 
    test('test getData', () async {
      // TODO
    });

    // Post a list of data points
    //
    //Future postData(String building, Data data) async 
    test('test postData', () async {
      // TODO
    });

    // Update a building
    //
    //Future updateBuildingInfo(String building, BuildingInfo buildingInfo) async 
    test('test updateBuildingInfo', () async {
      // TODO
    });

  });
}
