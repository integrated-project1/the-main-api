# openapi.model.Data

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**columns** | **List&lt;String&gt;** |  | [default to const []]
**index** | **List&lt;String&gt;** |  | [default to const []]
**data** | [**List&lt;List&lt;double&gt;&gt;**](List.md) |  | [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


