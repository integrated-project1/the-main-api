# openapi.model.BuildingInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**location** | **List&lt;double&gt;** |  | [optional] [default to const []]
**displayName** | **String** |  | [optional] 
**mapDatatype** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**datatypes** | **List&lt;String&gt;** |  | [optional] [default to const []]
**maxima** | **List&lt;double&gt;** |  | [optional] [default to const []]
**minima** | **List&lt;double&gt;** |  | [optional] [default to const []]
**lastAdded** | [**DateTime**](DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


