# openapi.api.DefaultApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBuilding**](DefaultApi.md#addBuilding) | **POST** /buildings | Add a building
[**deleteBuilding**](DefaultApi.md#deleteBuilding) | **DELETE** /buildings/{building} | Delete a building
[**getBuildingInfo**](DefaultApi.md#getBuildingInfo) | **GET** /buildings/{building} | Get information about the building
[**getBuildings**](DefaultApi.md#getBuildings) | **GET** /buildings | Get the list of buildings supported by this API
[**getData**](DefaultApi.md#getData) | **GET** /data/{building} | Get a list of data points as a JSON
[**postData**](DefaultApi.md#postData) | **POST** /data/{building} | Post a list of data points
[**updateBuildingInfo**](DefaultApi.md#updateBuildingInfo) | **PUT** /buildings/{building} | Update a building


# **addBuilding**
> addBuilding(buildingInfo)

Add a building

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var buildingInfo = BuildingInfo(); // BuildingInfo | Info about the building

try { 
    api_instance.addBuilding(buildingInfo);
} catch (e) {
    print("Exception when calling DefaultApi->addBuilding: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildingInfo** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteBuilding**
> deleteBuilding(building)

Delete a building

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var building = building_example; // String | Name of the building

try { 
    api_instance.deleteBuilding(building);
} catch (e) {
    print("Exception when calling DefaultApi->deleteBuilding: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getBuildingInfo**
> BuildingInfo getBuildingInfo(building)

Get information about the building

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var building = building_example; // String | Name of the building

try { 
    var result = api_instance.getBuildingInfo(building);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getBuildingInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 

### Return type

[**BuildingInfo**](BuildingInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getBuildings**
> List<String> getBuildings()

Get the list of buildings supported by this API

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.getBuildings();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getBuildings: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**List<String>**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getData**
> Data getData(building, firstDate, lastDate, datatypes, timestep, responseFormat)

Get a list of data points as a JSON

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var building = building_example; // String | Name of the building
var firstDate = 2017-07-21T17:32Z; // DateTime | Date of the first data point
var lastDate = 2017-07-21T17:33Z; // DateTime | Date of the last data point
var datatypes = TensionV12 TensionV23 TensionV31 I1 I2 I3; // String | A space separated string list of the requested datatypes
var timestep = 8.14; // num | Requested timestep, either 1 minute or 15 minutes. Default is 1 minute.
var responseFormat = responseFormat_example; // String | Format of the response, either JSON or csv. Default is JSON.

try { 
    var result = api_instance.getData(building, firstDate, lastDate, datatypes, timestep, responseFormat);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getData: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **firstDate** | **DateTime**| Date of the first data point | 
 **lastDate** | **DateTime**| Date of the last data point | 
 **datatypes** | **String**| A space separated string list of the requested datatypes | [optional] 
 **timestep** | **num**| Requested timestep, either 1 minute or 15 minutes. Default is 1 minute. | [optional] 
 **responseFormat** | **String**| Format of the response, either JSON or csv. Default is JSON. | [optional] 

### Return type

[**Data**](Data.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **postData**
> postData(building, data)

Post a list of data points

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var building = building_example; // String | Name of the building
var data = Data(); // Data | JSON representing a dataframe

try { 
    api_instance.postData(building, data);
} catch (e) {
    print("Exception when calling DefaultApi->postData: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **data** | [**Data**](Data.md)| JSON representing a dataframe | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateBuildingInfo**
> updateBuildingInfo(building, buildingInfo)

Update a building

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var building = building_example; // String | Name of the building
var buildingInfo = BuildingInfo(); // BuildingInfo | Info about the building

try { 
    api_instance.updateBuildingInfo(building, buildingInfo);
} catch (e) {
    print("Exception when calling DefaultApi->updateBuildingInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **buildingInfo** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

