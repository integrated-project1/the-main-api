# ElectricalEnergyAtAGlanceApi.BuildingInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**location** | **[Number]** |  | [optional] 
**displayName** | **String** |  | [optional] 
**mapDatatype** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**datatypes** | **[String]** |  | [optional] 
**maxima** | **[Number]** |  | [optional] 
**minima** | **[Number]** |  | [optional] 
**lastAdded** | **Date** |  | [optional] 


