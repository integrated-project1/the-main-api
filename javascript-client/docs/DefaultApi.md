# ElectricalEnergyAtAGlanceApi.DefaultApi

All URIs are relative to *http://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBuilding**](DefaultApi.md#addBuilding) | **POST** /buildings | Add a building
[**deleteBuilding**](DefaultApi.md#deleteBuilding) | **DELETE** /buildings/{building} | Delete a building
[**getBuildingInfo**](DefaultApi.md#getBuildingInfo) | **GET** /buildings/{building} | Get information about the building
[**getBuildings**](DefaultApi.md#getBuildings) | **GET** /buildings | Get the list of buildings supported by this API
[**getData**](DefaultApi.md#getData) | **GET** /data/{building} | Get a list of data points as a JSON
[**postData**](DefaultApi.md#postData) | **POST** /data/{building} | Post a list of data points
[**updateBuildingInfo**](DefaultApi.md#updateBuildingInfo) | **PUT** /buildings/{building} | Update a building



## addBuilding

> addBuilding(buildingInfo)

Add a building

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';
let defaultClient = ElectricalEnergyAtAGlanceApi.ApiClient.instance;
// Configure OAuth2 access token for authorization: Oauth2
let Oauth2 = defaultClient.authentications['Oauth2'];
Oauth2.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let buildingInfo = new ElectricalEnergyAtAGlanceApi.BuildingInfo(); // BuildingInfo | Info about the building
apiInstance.addBuilding(buildingInfo, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildingInfo** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

null (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## deleteBuilding

> deleteBuilding(building)

Delete a building

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';
let defaultClient = ElectricalEnergyAtAGlanceApi.ApiClient.instance;
// Configure OAuth2 access token for authorization: Oauth2
let Oauth2 = defaultClient.authentications['Oauth2'];
Oauth2.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let building = "building_example"; // String | Name of the building
apiInstance.deleteBuilding(building, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 

### Return type

null (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## getBuildingInfo

> BuildingInfo getBuildingInfo(building)

Get information about the building

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let building = "building_example"; // String | Name of the building
apiInstance.getBuildingInfo(building, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 

### Return type

[**BuildingInfo**](BuildingInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getBuildings

> [String] getBuildings()

Get the list of buildings supported by this API

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
apiInstance.getBuildings((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

**[String]**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getData

> Data getData(building, firstDate, lastDate, opts)

Get a list of data points as a JSON

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let building = "building_example"; // String | Name of the building
let firstDate = 2017-07-21T17:32Z; // Date | Date of the first data point
let lastDate = 2017-07-21T17:33Z; // Date | Date of the last data point
let opts = {
  'datatypes': TensionV12 TensionV23 TensionV31 I1 I2 I3, // String | A space separated string list of the requested datatypes
  'timestep': 3.4, // Number | Requested timestep, either 1 minute or 15 minutes. Default is 1 minute.
  'responseFormat': "responseFormat_example" // String | Format of the response, either JSON or csv. Default is JSON.
};
apiInstance.getData(building, firstDate, lastDate, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **firstDate** | **Date**| Date of the first data point | 
 **lastDate** | **Date**| Date of the last data point | 
 **datatypes** | **String**| A space separated string list of the requested datatypes | [optional] 
 **timestep** | **Number**| Requested timestep, either 1 minute or 15 minutes. Default is 1 minute. | [optional] 
 **responseFormat** | **String**| Format of the response, either JSON or csv. Default is JSON. | [optional] 

### Return type

[**Data**](Data.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv


## postData

> postData(building, data)

Post a list of data points

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';
let defaultClient = ElectricalEnergyAtAGlanceApi.ApiClient.instance;
// Configure OAuth2 access token for authorization: Oauth2
let Oauth2 = defaultClient.authentications['Oauth2'];
Oauth2.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let building = "building_example"; // String | Name of the building
let data = new ElectricalEnergyAtAGlanceApi.Data(); // Data | JSON representing a dataframe
apiInstance.postData(building, data, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **data** | [**Data**](Data.md)| JSON representing a dataframe | 

### Return type

null (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## updateBuildingInfo

> updateBuildingInfo(building, buildingInfo)

Update a building

### Example

```javascript
import ElectricalEnergyAtAGlanceApi from 'electrical_energy_at_a_glance_api';
let defaultClient = ElectricalEnergyAtAGlanceApi.ApiClient.instance;
// Configure OAuth2 access token for authorization: Oauth2
let Oauth2 = defaultClient.authentications['Oauth2'];
Oauth2.accessToken = 'YOUR ACCESS TOKEN';

let apiInstance = new ElectricalEnergyAtAGlanceApi.DefaultApi();
let building = "building_example"; // String | Name of the building
let buildingInfo = new ElectricalEnergyAtAGlanceApi.BuildingInfo(); // BuildingInfo | Info about the building
apiInstance.updateBuildingInfo(building, buildingInfo, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **building** | **String**| Name of the building | 
 **buildingInfo** | [**BuildingInfo**](BuildingInfo.md)| Info about the building | 

### Return type

null (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

