# ElectricalEnergyAtAGlanceApi.Data

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**columns** | **[String]** |  | 
**index** | **[String]** |  | 
**data** | **[[Number]]** |  | 


