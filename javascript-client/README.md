# electrical_energy_at_a_glance_api

ElectricalEnergyAtAGlanceApi - JavaScript client for electrical_energy_at_a_glance_api
API to get electrical data at Uliege
This SDK is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/), please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install electrical_energy_at_a_glance_api --save
```

Finally, you need to build the module:

```shell
npm run build
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

To use the link you just defined in your project, switch to the directory you want to use your electrical_energy_at_a_glance_api from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

Finally, you need to build the module:

```shell
npm run build
```

#### git

If the library is hosted at a git repository, e.g.https://github.com/GIT_USER_ID/GIT_REPO_ID
then install it via:

```shell
    npm install GIT_USER_ID/GIT_REPO_ID --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var ElectricalEnergyAtAGlanceApi = require('electrical_energy_at_a_glance_api');

var defaultClient = ElectricalEnergyAtAGlanceApi.ApiClient.instance;
// Configure OAuth2 access token for authorization: Oauth2
var Oauth2 = defaultClient.authentications['Oauth2'];
Oauth2.accessToken = "YOUR ACCESS TOKEN"

var api = new ElectricalEnergyAtAGlanceApi.DefaultApi()
var buildingInfo = new ElectricalEnergyAtAGlanceApi.BuildingInfo(); // {BuildingInfo} Info about the building
var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
api.addBuilding(buildingInfo, callback);

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost/1.0.0*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**addBuilding**](docs/DefaultApi.md#addBuilding) | **POST** /buildings | Add a building
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**deleteBuilding**](docs/DefaultApi.md#deleteBuilding) | **DELETE** /buildings/{building} | Delete a building
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**getBuildingInfo**](docs/DefaultApi.md#getBuildingInfo) | **GET** /buildings/{building} | Get information about the building
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**getBuildings**](docs/DefaultApi.md#getBuildings) | **GET** /buildings | Get the list of buildings supported by this API
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**getData**](docs/DefaultApi.md#getData) | **GET** /data/{building} | Get a list of data points as a JSON
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**postData**](docs/DefaultApi.md#postData) | **POST** /data/{building} | Post a list of data points
*ElectricalEnergyAtAGlanceApi.DefaultApi* | [**updateBuildingInfo**](docs/DefaultApi.md#updateBuildingInfo) | **PUT** /buildings/{building} | Update a building


## Documentation for Models

 - [ElectricalEnergyAtAGlanceApi.BuildingInfo](docs/BuildingInfo.md)
 - [ElectricalEnergyAtAGlanceApi.Data](docs/Data.md)


## Documentation for Authorization



### Oauth2


- **Type**: OAuth
- **Flow**: implicit
- **Authorization URL**: /login
- **Scopes**: 
  - write:buildings: Add a building
  - update:buildings: Update a building
  - delete:buildings: Delete a building
  - read:data: Read more than 30 days of data
  - write:data: Update the database

