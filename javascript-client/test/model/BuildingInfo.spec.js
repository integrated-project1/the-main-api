/**
 * Electrical energy at a glance API
 * API to get electrical data at Uliege
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ElectricalEnergyAtAGlanceApi);
  }
}(this, function(expect, ElectricalEnergyAtAGlanceApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('BuildingInfo', function() {
    it('should create an instance of BuildingInfo', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo();
      expect(instance).to.be.a(ElectricalEnergyAtAGlanceApi.BuildingInfo);
    });

    it('should have the property name (base name: "name")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"name": "buildy"});
      expect(instance.name).to.be("buildy");
    });

    it('should have the property location (base name: "location")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"location": [5,9]});
      expect(instance.location).to.eql([5,9]);
    });

    it('should have the property displayName (base name: "displayName")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"displayName": "cos"});
      expect(instance.displayName).to.be("cos");
    });

    it('should have the property mapDatatype (base name: "mapDatatype")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"mapDatatype": "cos"});
      expect(instance.mapDatatype).to.be("cos");
    });

    it('should have the property type (base name: "type")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"type": "production"});
      expect(instance.type).to.be("production");
    });

    it('should have the property datatypes (base name: "datatypes")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"datatypes": ["voltage_12", "cos"]});
      expect(instance.datatypes).to.eql(["voltage_12", "cos"]);
    });

    it('should have the property maxima (base name: "maxima")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"maxima": [0,0,0]});
      expect(instance.maxima).to.eql([0,0,0]);
    });

    it('should have the property minima (base name: "minima")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"minima": [0,0,0]});
      expect(instance.minima).to.eql([0,0,0]);
    });

    it('should have the property lastAdded (base name: "last_added")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.BuildingInfo.constructFromObject({"last_added": "2017-02-03T00:00:00Z"});
      expect(instance.last_added).to.be.a(Date);
    });

  });

}));
