/**
 * Electrical energy at a glance API
 * API to get electrical data at Uliege
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.ElectricalEnergyAtAGlanceApi);
  }
}(this, function(expect, ElectricalEnergyAtAGlanceApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new ElectricalEnergyAtAGlanceApi.Data();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('Data', function() {
    it('should create an instance of Data', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.Data();
      expect(instance).to.be.a(ElectricalEnergyAtAGlanceApi.Data);
    });

    it('should have the property columns (base name: "columns")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.Data.constructFromObject({"columns": [5.6, 9.7]});
      expect(instance.columns).to.eql([5.6, 9.7]);
    });

    it('should have the property index (base name: "index")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.Data.constructFromObject({"index": ["2017-02-03T00:00:00Z", "2017-02-03T00:00:00Z"]});
      expect(instance.index).to.eql(["2017-02-03T00:00:00Z", "2017-02-03T00:00:00Z"])
    });

    it('should have the property data (base name: "data")', function() {
      var instance = new ElectricalEnergyAtAGlanceApi.Data.constructFromObject({"data": [[5.6, 9.7], [5.6, 9.7]]});
      expect(instance.data).to.eql([[5.6, 9.7], [5.6, 9.7]]);
    });

  });

}));
